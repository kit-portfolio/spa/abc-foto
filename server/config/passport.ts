import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt';
import User from '../models/User.model';
import keys from './keys';
import messages from './messages';

interface IOpts {
  jwtFromRequest: string,
  secretOrKey?: string,
}

/**
 * Validation helper method.
 * @param   passport - auth data
 */
export async function configurePassport(passport) {
  const opts: IOpts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: keys.secretOrKey,
  };

  passport.use(
      'jwt',
      new JwtStrategy(opts, (jwtPayload, done) => {
        User.findById(jwtPayload.id)
            .then((customer) => {
              if (customer) {
                return done(null, customer);
              }
              return done(null, false);
            })
            .catch((err) => console.log(err));
      }),
  );

  passport.use(
      'jwt-admin',
      new JwtStrategy(opts, (jwtPayload, done) => {
        User.findById(jwtPayload.id)
            .then((user) => {
              if (user && user.isAdmin) {
                return done(null, user);
              }
              return done(null, false, {
                message: messages.errors.generic.permissionsLack,
              });
            })
            .catch((e) => console.log(e));
      }),
  );
}

// Modules
import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {ToastContainer, toast} from 'material-react-toastify';
import 'material-react-toastify/dist/ReactToastify.css';
// Components
import TopBar from './components/top-bar/top-bar';
import Main from './components/main/ main';
import Footer from './components/footer/footer';
import ModalController from './components/misc/modal-controller/modal-controller';
// Misc
import {AppContainer} from './theme/common';
import {logIn} from './store/auth/auth.actions';

toast.configure();

/**
 * App component is kinda kingpin here.
 * @remarks This is a wise remark.
 * @returns JSX.Element
 */
export default function App(): JSX.Element {
  const dispatch = useDispatch();
  useEffect(() => logIn(dispatch), [dispatch]);

  return (
    <AppContainer>
      <TopBar/>
      <Main/>
      <Footer/>
      <ModalController/>
      <ToastContainer
        position="bottom-left"
        autoClose={3000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover/>
    </AppContainer>
  );
};

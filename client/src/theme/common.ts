import styled, {createGlobalStyle} from 'styled-components';
import {PALETTE} from './theme';
import {ToastOptions} from 'material-react-toastify';

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    user-select: none;
    font-family: 'Roboto', sans-serif;
  }

  p {
    margin: 0;
  }
`;

export const AppContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  min-height: 100vh;
`;

export const ContentBox = styled.div`
  width: 1200px;
  margin: 0 auto;
  text-align: center;
`;

export const simpleForm = styled.form`
  border: 1px solid ${PALETTE.NOBEL};
  background-color: ${PALETTE.WHITE};
  border-radius: 20px;
  height: 32px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 3px;
  box-sizing: border-box;

  & input {
    border: none;
    margin: 0 10px;
    flex-grow: 1;
  }

  & svg {
    height: 100%;
    width: auto;
  }

  & circle {
    transition: 1s;
    fill: ${PALETTE.NOBEL};
  }

  &:focus-within {
    & circle {
      transition: 1s;
      fill: ${PALETTE.APPLE};
    }
  }
`;

export const toastConfig: ToastOptions = {
  position: 'bottom-left',
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

// Modules
import React from 'react';
import {TODO_ANY} from '../../../../types/misc';
import {Header} from './checkout-form.style';
import {
  TextInput,
  Label,
  LabelText,
  ValidationMessage,
} from '../../../../theme/form';
import {PLACEHOLDER} from '../../../../config/form';

interface ICheckoutFormProps {
  formController: TODO_ANY
}

/**
 * CheckoutForm is the place where order information is collected for order execution.
 * @prop formController - Formik object controlling form fields.
 * @returns JSX.Element
 */
export default function CheckoutForm(props: ICheckoutFormProps): JSX.Element {
  const {formController} = props;

  return (
    <form action="#" onSubmit={formController.handleSubmit}>
      <Header>Контактные данные</Header>
      <Label htmlFor="username">
        <LabelText>Имя получателя</LabelText>
        {formController.errors.name && formController.touched.name ?
          (<ValidationMessage>{formController.errors.name}</ValidationMessage>) :
          null}
        <TextInput
          id='username'
          type='text'
          name='name'
          value={formController.values.name}
          onChange={formController.handleChange}
          placeholder={PLACEHOLDER.NAME}
        />
      </Label>
      <Label htmlFor="phone">
        <LabelText>Телефон</LabelText>
        {formController.errors.phone && formController.touched.phone ?
          (<ValidationMessage>{formController.errors.phone}</ValidationMessage>) :
          null}
        <TextInput
          id='phone'
          type='tel'
          name='phone'
          value={formController.values.phone}
          onChange={formController.handleChange}
          placeholder={PLACEHOLDER.PHONE}
        />
      </Label>
      <Label htmlFor="email">
        <LabelText>E-mail</LabelText>
        {formController.errors.email && formController.touched.email ?
          (<ValidationMessage>{formController.errors.email}</ValidationMessage>) :
          null}
        <TextInput
          id='email'
          type='text'
          name='email'
          value={formController.values.email}
          onChange={formController.handleChange}
          placeholder={PLACEHOLDER.EMAIL}
        />
      </Label>
    </form>
  );
}

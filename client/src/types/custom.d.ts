import {TODO_ANY} from './misc';

/**
 * @hint Module intended to allow svg import as React components.
 **/
declare module '*.svg' {
  const content: TODO_ANY;
    export default content;
}
// TODO: Causes TS compilation issues. Investigate?

export interface IContact {
  _id: string,
  tag: string,
  label: string,
  href: string,
  image: string,
  enabled: boolean,
}

import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from '../../../theme/theme';
import {ContentBox} from '../../../theme/common';

export const Grid = styled(ContentBox)`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: 
    "checkout-header checkout-header"
    "checkout-form cart-widget";
  grid-column-gap: 160px;
  margin-bottom: 30px;
`;

export const Header = styled.h2`
  ${FONT_SIZE.HEADER};
  text-transform: uppercase;
  color: ${PALETTE.BLACK};
  grid-area: checkout-header;
`;

import {URN} from '../../config/constants';

export enum BREADCRUMBS_ACTION {
  CART = 'SET CART BREADCRUMBS',
  CHECKOUT = 'SET CHECKOUT BREADCRUMBS',
  PROFILE = 'SET PROFILE BREADCRUMBS',
  CLEAR = 'CLEAR BREADCRUMBS',
}


export interface IBreadcrumb {
    label: string,
    href?: URN,
}

export type BreadcrumbsStore = Array<IBreadcrumb> | [];

import styled from 'styled-components';
import {PALETTE, FONT_SIZE} from '../../../../theme/theme';

export const Container = styled.div`
  background: ${PALETTE.LIGHT_GRAY};
  padding: 90px;
  display: block;
`;

export const Header = styled.h2`
  background: ${PALETTE.LIGHT_GRAY};
  font-size: ${FONT_SIZE.HEADER};
  text-transform: uppercase;
  color: ${PALETTE.PALE_SKY};
  text-align: left;
`;

export const SalesGrid = styled.div`
  display: grid;
  grid-template: 
            "sale-1 sale-1 sale-2" 210px
            "sale-1 sale-1 sale-3" 210px
            "sale-4 sale-5 sale-6" 210px / 1fr 1fr 1fr;
  gap: 20px;
`;

export const SalePoster = styled.img`
  width: 100%;
  height: 100%;
`;

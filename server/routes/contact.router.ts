import {Router as router} from 'express';
import {
  createContact,
  getContacts,
  getContactById,
  updateContact,
  deleteContact,
} from '../controllers/contact.controller';

export const contact: router = router({
  strict: true,
});

contact.post(
    '/',
    createContact,
);

contact.get(
    '/',
    getContacts,
);

contact.get(
    '/:id',
    getContactById,
);

contact.put(
    '/:id',
    updateContact,
);

contact.delete(
    '/:id',
    deleteContact,
);

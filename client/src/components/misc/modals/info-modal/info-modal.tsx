import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import Modal from 'react-modal';
import {closeModal} from '../../../../store/modal-controller/modal-controller.actions';
import {
  Title,
  Message,
  ActionsBox,
  CloseButton,
  OverlayStyle,
  ContentStyle,
} from './info-modal.style';

export interface IInfoModalProps {
  image?: JSX.Element
  title: string
  message: string
  actions?: Array<JSX.Element>
}

interface IState {
  modalController: {
    config: IInfoModalProps
  }
}

/**
 * Modal window for displaying informational messages
 * @remarks
 * This component is controlled by ModalController.
 * Contents are based on Redux values.
 * @returns JSX.Element
 */
export default function InfoModal(): JSX.Element {
  const {title, message, actions, image} = useSelector((state: IState) => state.modalController.config);
  const dispatch = useDispatch();

  /**
   * Helper function intended to render actions box
   * @returns JSX.Element | null
   */
  function renderActions(): JSX.Element | null {
    if (!actions) {
      return null;
    }
    return (<ActionsBox>
      {actions && actions.map((item) => item)}
    </ActionsBox>);
  }

  return (
    <Modal
      isOpen={true}
      style={{
        overlay: OverlayStyle,
        content: ContentStyle,
      }}
      shouldCloseOnOverlayClick
      shouldCloseOnEsc
      onRequestClose={() => dispatch(closeModal())}
    >
      <CloseButton onClick={() => dispatch(closeModal())}/>
      {image}
      <Title>{title}</Title>
      <Message>{message}</Message>
      {renderActions()}
    </Modal>
  );
}

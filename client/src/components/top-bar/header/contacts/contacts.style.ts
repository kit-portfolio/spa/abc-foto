import styled from 'styled-components';
import {PALETTE} from '../../../../theme/theme';
// @ts-ignore
import {ReactComponent as Chevron} from '../../../../resources/chevron-simple.svg';

export const Container = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
  cursor: pointer;
  
  &:hover {
    input {
      transition: 0.3s;
      color: ${PALETTE.APPLE};
    }
    
    svg {
      transition: 0.3s;
      fill: ${PALETTE.APPLE};
    }
  }
`;

export const Selector = styled.input`
  width: 75px;
  text-align: center;
  user-select: none;
  border: none;
  text-transform: uppercase;
  color: ${PALETTE.PALE_SKY};
  transition: 0.3s;
  background: none;
`;

export const SelectorChevron = styled(Chevron)`
  fill: ${PALETTE.PALE_SKY};
  height: 20px; width: 20px;
`;

export const Dropdown = styled.div`
  background-color: white;
  box-shadow: 0 0 2px 0 rgba(34, 60, 80, 0.4);
  position: absolute;
  top: 25px;
  z-index: 10;
`;

export const PhoneNumberContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
  transition: 0.3s;
  
  &:hover {
    transition: 0.3s;
    background-color: ${PALETTE.LIGHT_GRAY};
  }
`;

export const PhoneImage = styled.img`
  display: inline-block;
  margin-right: 10px;
  width: 20px;
  height: 20px;
`;

export const PhoneNumber = styled.a`
  display: inline-block;
  text-decoration: none;
  color: ${PALETTE.PALE_SKY};
`;

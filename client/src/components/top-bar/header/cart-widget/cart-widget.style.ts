import styled from 'styled-components';
// @ts-ignore
import {ReactComponent as CartIcon} from '../../../../resources/cart.svg';
import {widgetPictogramDimensions, WidgetWrapper} from '../header.style';
import {PALETTE} from '../../../../theme/theme';

export const CartWidgetWrapper = styled(WidgetWrapper)`
  position: relative;
  padding-left: 10px;
`;

export const CartLogo = styled(CartIcon)`
  ${widgetPictogramDimensions};
  margin-right: 10px;
  
  .background-circle {
    fill: ${PALETTE.NOBEL}
  }
`;

export const CartBadge = styled.div`
  background-color: ${PALETTE.APPLE};
  color: ${PALETTE.WHITE};
  width: 20px; height: 20px;
  line-height: 20px;
  border-radius: 50%;
  position: absolute;
  bottom: 2px;
  left: 0;
`;

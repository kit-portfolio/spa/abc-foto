import styled from 'styled-components';
import {PALETTE, FONT_SIZE} from '../../../theme/theme';
// @ts-ignore
import {ReactComponent as LogoutSVG} from '../../../resources/logout.svg';

export const Header = styled.h2`
  ${FONT_SIZE.HEADER};
  color: ${PALETTE.BLACK};  
  text-transform: uppercase;
  text-align: left;
`;

export const LogoutIcon = styled(LogoutSVG)`
  height: 64px;
  width: 64px;
  fill: ${PALETTE.NOBEL};
`;

export const Grid = styled.div`
  display: grid;
  grid-gap: 180px;
  grid-template-columns: 240px auto;
  min-height: 500px;
  margin-bottom: 50px;
`;

interface ITabProps {
  isActive?: boolean;
}

export const Tab = styled.div<ITabProps>`
  background-color: ${(props) => (props.isActive ? PALETTE.PALE_SKY : PALETTE.LIGHT_GRAY)};
  color: ${(props) => (props.isActive ? PALETTE.WHITE : PALETTE.BLACK)};
  padding: 28px 0;
  cursor: pointer;
  text-align: center;
  border-bottom: 1px solid ${PALETTE.NOBEL};
  transition: 0.5s;
  
  &:hover {
    background-color: ${PALETTE.PALE_SKY};
    color: ${PALETTE.WHITE};
    transition: 0.5s;
  }
`;

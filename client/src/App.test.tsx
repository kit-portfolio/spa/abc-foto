import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';

describe.skip('App component', () => {
  test('App container is ok', () => {
    render(<App/>);
    const target = screen.getByTestId('app');
    expect(target).toBeInTheDocument();
  });
});

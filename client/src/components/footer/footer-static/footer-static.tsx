// Modules
import React from 'react';
// Component resources
import {FooterLink, footerLinksStyle, Text} from '../footer.style';
import {IFooterStaticLinksBlock, IStaticLink} from '../../../types/misc';

interface IFooterStaticLinksProps {
  content: IFooterStaticLinksBlock
}
/**
 * This component renders a block of static informational links in footer.
 * @prop content - An array of objects describing footer static pages links.
 * @returns JSX.Element
 */
export default function FooterStaticLinks(props: IFooterStaticLinksProps): JSX.Element {
  const {title, links} = props.content;

  /**
   * Helper function creating an array of links.
   * @returns JSX.Element[]
   */
  function renderStaticLinks(target: Array<IStaticLink>):JSX.Element[] {
    return target.map((item) => {
      return <FooterLink key={item.id} to={item.href}>{item.title}</FooterLink>;
    },
    );
  }

  return (
    <div>
      <Text style={footerLinksStyle()}>{title}</Text>
      {renderStaticLinks(links)}
    </div>
  );
}

import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

interface IOrder extends Document {
  userId?: string,
  comment: string,
  client: IClient,
  products: Array<string>,
  shipping: IShipping,
  total: number,
  status: string,
  date: string,
  orderId: string,
}

interface IClient {
  firstName: string,
  lastName: string,
  phone: string,
  email: string,
}

interface IShipping {
  type: string,
  address: string,
  price: number,
}

const OrderSchema: Schema = new Schema(
    {
      userId: {
        type: String,
      },
      comment: {
        type: String,
        required: true,
      },
      client: {
        type: {
          firstName: String,
          lastName: String,
          phone: String,
          email: String,
        },
        required: true,
      },
      products: {
        type: [String],
        required: true,
      },
      shipping: {
        type: {
          type: String,
          address: String,
          price: Number,
        },
      },
      total: {
        type: Number,
        required: true,
      },
      status: {
        type: String,
        required: true,
      },
      date: {
        type: Date,
        default: Date.now,
        required: true,
      },
      orderId: {
        type: String,
        required: true,
      },
    },
    {strict: false},
);

export default model<IOrder>(MODEL.ORDER, OrderSchema);

import React from 'react';
import {Container, Header, Message} from './error-page.style';

interface IErrorPageProps {
  image?: JSX.Element,
  header?: string,
  message: string | JSX.Element,
}

/**
 * The unified error page.
 * @prop image - JSX element intended to be shown as event-specific pictogram on error page.
 * @prop header - Header message
 * @prop message - Main message, describing error event in details. Is required to enforce UX.
 * @returns JSX.Element
 */
export default function ErrorPage(props: IErrorPageProps): JSX.Element {
  const {image, header, message} = props;

  return (
    <Container>
      {image}
      {header && <Header>{header}</Header>}
      <Message>{message}</Message>
    </Container>
  );
}

// TODO: doesn't fit all the height on dedicated page

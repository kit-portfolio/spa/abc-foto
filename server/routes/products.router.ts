import {Router as router} from 'express';
import {
  addProduct,
  updateProduct,
  getProducts,
  getProductById,
} from '../controllers/products.controller';

export const products: router = router({
  strict: true,
});

products.post(
    '/',
    addProduct,
);

products.put(
    '/:id',
    updateProduct,
);

products.get(
    '/',
    getProducts,
);

products.get(
    '/:id',
    getProductById,
);

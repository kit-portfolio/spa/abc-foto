// Modules
import React from 'react';
import {TODO_ANY} from '../../../../types/misc';
// Styles
import {Container,
  Header,
  ContentBox,
  Shipping,
  OrderTotal,
  ItemsBox,
  Placeholder,
} from './cart-widget.style';
import {ActionButton} from '../../../../theme/form';


interface ICartWidgetProps {
  formController: TODO_ANY
}

/**
 * Card widget displays shopping cart contents.
 * @prop formController - Formik object controlling the 'submit button'.
 * @returns JSX.Element
 */
export default function CartWidget(props: ICartWidgetProps): JSX.Element {
  const {formController} = props;

  /**
   * This helper function renders a pack of cart items placeholders.
   * @remarks Must be deleted when actual component will be ready
   * @returns JSX.Element[]
   */
  function renderPlaceholder(): JSX.Element[] {
    const md5 = require('md5');
    const arr = ['First', 'Second', 'Third', 'Fourth', 'Fifth'];
    return arr.map((item) => <Placeholder key={md5(item)}>{`${item} placeholder`}</Placeholder>);
  }
  // TODO: remove when cart item component will be ready

  return (
    <Container>
      <Header>Товары в корзине</Header>
      <ContentBox>
        <ItemsBox>
          {renderPlaceholder()}
        </ItemsBox>
        <Shipping>Доставка  - 150 грн</Shipping>
        <OrderTotal>Сумма заказа: 16 619 грн</OrderTotal>
        <ActionButton onClick={formController.submitForm}>Подтвердить</ActionButton>
      </ContentBox>
    </Container>
  );
}

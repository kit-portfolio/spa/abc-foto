import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from './theme';
import {darken} from 'polished';

interface IActionButtonProps {
  color?: string,
  textColor?: string,
}

export const ActionButton = styled.div<IActionButtonProps>`
  background-color: ${(props) => (props.color) ? props.color : PALETTE.APPLE};
  color: ${(props) => (props.textColor) ? props.textColor : PALETTE.WHITE};
  width: 80%;
  margin: 0 auto;
  padding: 15px;
  box-sizing: border-box;
  text-transform: uppercase;
  text-align: center;
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;

  &:hover {
    background-color: ${(props) => (props.color) ?
      darken(0.05, props.color) :
      darken(0.05, PALETTE.APPLE)};
    transition: 0.3s;
  }
`;

export const Label = styled.label`
  display: block;
  text-align: left;
`;

export const LabelText = styled.p`
  ${FONT_SIZE.TEXT};
  color: ${PALETTE.BLACK};
  margin-bottom: 5px;
  display: inline-block;
`;

export const ValidationMessage = styled(LabelText)`
  color: ${PALETTE.ALIZARIN_CRIMSON};

  &::before {
    content: "- ";
    margin-left: 5px;
  }
`;

const inputStyle = `
  width: 100%;
  border-radius: 5px;
  margin-bottom: 15px;
  border: 1px solid ${PALETTE.NOBEL};
  padding: 15px;
  box-sizing: border-box;
  color: ${PALETTE.PALE_SKY};
  
  &::placeholder {
    color: ${PALETTE.NOBEL};
  }
`;

export const TextInput = styled.input`
  ${inputStyle};
`;

interface IPasswordInput {
  hidePassword: boolean,
}

export const PasswordInput = styled.div<IPasswordInput>`
  position: relative;

  input {
    ${inputStyle};
  }

  svg {
    position: absolute;
    top: 14px;
    right: 20px;
    fill: ${(props) => props.hidePassword ? PALETTE.NOBEL : PALETTE.APPLE}
  }
`;

export const DateInputWrapper = styled.div`
  ${inputStyle};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Dropdown = styled.select`
  ${inputStyle};
`;

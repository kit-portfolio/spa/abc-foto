const cameras = [
  {
    name: 'PowerShot SX430 IS Black',
    manufacturer: 'Canon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/camera/powershot-sx430-1.jpg',
        preview: '/img/products/photo/camera/powershot-sx430-1_s.jpg',
      },
      {
        image: '/img/products/photo/camera/powershot-sx430-2.jpg',
        preview: '/img/products/photo/camera/powershot-sx430-2_s.jpg',
      },
      {
        image: '/img/products/photo/camera/powershot-sx430-3.jpg',
        preview: '/img/products/photo/camera/powershot-sx430-3_s.jpg',
      },
      {
        image: '/img/products/photo/camera/powershot-sx430-4.jpg',
        preview: '/img/products/photo/camera/powershot-sx430-4_s.jpg',
      },
    ],
    quantity: 900,
    category: 'photo',
    type: 'video-camera',
    price: 7599,
    salePrice: 6999,
    characteristics: [
      {
        title: 'display',
        value: '3.5',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'Зеркальньій',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CCD',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'Optical',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '45x',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'E-M5 mark III Body Silver',
    manufacturer: 'Olympus',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/camera/e-m5-mark-III-1.jpg',
        preview: '/img/products/photo/camera/e-m5-mark-III-1_s.jpg',
      },
      {
        image: '/img/products/photo/camera/e-m5-mark-III-2.jpg',
        preview: '/img/products/photo/camera/e-m5-mark-III-2_s.jpg',
      },
      {
        image: '/img/products/photo/camera/e-m5-mark-III-3.jpg',
        preview: '/img/products/photo/camera/e-m5-mark-III-3_s.jpg',
      },
      {
        image: '/img/products/photo/camera/e-m5-mark-III-4.jpg',
        preview: '/img/products/photo/camera/e-m5-mark-III-4_s.jpg',
      },
      {
        image: '/img/products/photo/camera/e-m5-mark-III-5.jpg',
        preview: '/img/products/photo/camera/e-m5-mark-III-5_s.jpg',
      },
      {
        image: '/img/products/photo/camera/e-m5-mark-III-6.jpg',
        preview: '/img/products/photo/camera/e-m5-mark-III-6_s.jpg',
      },
    ],
    quantity: 73,
    category: 'photo',
    type: 'camera',
    price: 36495,
    salePrice: null,
    characteristics: [
      {
        title: 'display',
        value: '3',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'Системний',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'Digital',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: 'none',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'Coolpix B600 Red',
    manufacturer: 'Nikon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/camera/coolpix-b600-1.jpg',
        preview: '/img/products/photo/camera/coolpix-b600-1_s.jpg',
      },
      {
        image: '/img/products/photo/camera/coolpix-b600-2.jpg',
        preview: '/img/products/photo/camera/coolpix-b600-2_s.jpg',
      },
      {
        image: '/img/products/photo/camera/coolpix-b600-3.jpg',
        preview: '/img/products/photo/camera/coolpix-b600-3_s.jpg',
      },
      {
        image: '/img/products/photo/camera/coolpix-b600-4.jpg',
        preview: '/img/products/photo/camera/coolpix-b600-4_s.jpg',
      },
    ],
    quantity: 19,
    category: 'photo',
    type: 'camera',
    price: 11699,
    salePrice: null,
    characteristics: [
      {
        title: 'display',
        value: '3',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'super zoom',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'optical',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '60',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'Coolpix W300',
    manufacturer: 'Nikon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/camera/coolpix-w300-1.jpg',
        preview: '/img/products/photo/camera/coolpix-w300-1_s.jpg',
      },
      {
        image: '/img/products/photo/camera/coolpix-w300-2.jpg',
        preview: '/img/products/photo/camera/coolpix-w300-2_s.jpg',
      },
      {
        image: '/img/products/photo/camera/coolpix-w300-3.jpg',
        preview: '/img/products/photo/camera/coolpix-w300-3_s.jpg',
      },
    ],
    quantity: 1,
    category: 'photo',
    type: 'video-camera',
    price: 12399,
    salePrice: 9899,
    characteristics: [
      {
        title: 'display',
        value: '2.95',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'waterproof',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'Digital',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '5',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'KVR-005 Bear',
    manufacturer: 'Xoko',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/camera/kvr-005-1.jpg',
        preview: '/img/products/photo/camera/kvr-005-1_s.jpg',
      },
      {
        image: '/img/products/photo/camera/kvr-005-2.jpg',
        preview: '/img/products/photo/camera/kvr-005-2_s.jpg',
      },
    ],
    quantity: 0,
    category: 'photo',
    type: 'camera',
    price: 699,
    salePrice: null,
    characteristics: [
      {
        title: 'display',
        value: '2',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'compact',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'none',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: 'none',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'EOS M50 EF-M',
    manufacturer: 'Canon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/eos-m50-ef-m-1-s.jpg',
        image: '/img/products/photo/camera/eos-m50-ef-m-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/eos-m50-ef-m-2-s.jpg',
        image: '/img/products/photo/camera/eos-m50-ef-m-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/eos-m50-ef-m-3-s.jpg',
        image: '/img/products/photo/camera/eos-m50-ef-m-3.jpg',
      }, {
        preview: '/img/products/photo/camera/eos-m50-ef-m-4-s.jpg',
        image: '/img/products/photo/camera/eos-m50-ef-m-4.jpg',
      },
    ],
    quantity: 318,
    type: 'camera',
    category: 'video-camera',
    price: 14256,
    salePrice: 15000,
    characteristics: [
      {
        title: 'display',
        value: '3.5-6.3',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'system',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'optical',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '3x',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'Alpha A6000 kit',
    manufacturer: 'Sony',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/alpha-a6000-kit-1-s.jpg',
        image: '/img/products/photo/camera/alpha-a6000-kit-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/alpha-a6000-kit-2-s.jpg',
        image: '/img/products/photo/camera/alpha-a6000-kit-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/alpha-a6000-kit-3-s.jpg',
        image: '/img/products/photo/camera/alpha-a6000-kit-3.jpg',
      },
    ],
    quantity: 219,
    type: 'camera',
    category: 'photo',
    price: 7340,
    salePrice: 9500,
    characteristics: [
      {
        title: 'display',
        value: '16-50',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'system',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'Digital',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '3x',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'X-Pro3 Body Black ',
    manufacturer: 'FUJIFILM ',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/x-pro30-body-black-1-s.jpg',
        image: '/img/products/photo/camera/x-pro30-body-black-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/x-pro30-body-black-2-s.jpg',
        image: '/img/products/photo/camera/x-pro30-body-black-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/x-pro30-body-black-3-s.jpg',
        image: '/img/products/photo/camera/x-pro30-body-black-3.jpg',
      },
    ],
    quantity: 384,
    type: 'camera',
    category: 'video-camera',
    price: 54795,
    salePrice: 56000,
    characteristics: [
      {
        title: 'display',
        value: '23.5 х 15.6',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'system',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'none',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: 'none',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'LUMIX DC-FT7EE-K Black',
    manufacturer: 'PANASONIC ',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-1-s.jpg',
        image: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-2-s.jpg',
        image: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-3-s.jpg',
        image: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-3.jpg',
      },
      {
        preview: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-4-s.jpg',
        image: '/img/products/photo/camera/lumix-dc-ft7ee-k-black-4.jpg',
      },
    ],
    quantity: 174,
    type: 'camera',
    category: 'photo',
    price: 11999,
    salePrice: 13000,
    characteristics: [
      {
        title: 'display',
        value: '6,17 x 4,55 ',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'waterproof',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'MOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'optical',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '4.6x',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'EOS R6 body RUK/SEE',
    manufacturer: 'Canon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/eos-r6-body-ruk-see-1-s.jpg',
        image: '/img/products/photo/camera/eos-r6-body-ruk-see-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/eos-r6-body-ruk-see-2-s.jpg',
        image: '/img/products/photo/camera/eos-r6-body-ruk-see-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/eos-r6-body-ruk-see-3-s.jpg',
        image: '/img/products/photo/camera/eos-r6-body-ruk-see-3.jpg',
      },
      {
        preview: '/img/products/photo/camera/eos-r6-body-ruk-see-4-s.jpg',
        image: '/img/products/photo/camera/eos-r6-body-ruk-see-4.jpg',
      },
      {
        preview: '/img/products/photo/camera/eos-r6-body-ruk-see-5-s.jpg',
        image: '/img/products/photo/camera/eos-r6-body-ruk-see-5.jpg',
      },
    ],
    quantity: 92,
    type: 'camera',
    category: 'photo',
    price: 89399,
    salePrice: 92000,
    characteristics: [
      {
        title: 'display',
        value: '35.9 х 23.9',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'system',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'with matrix shift',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '0.21х',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'Coolpix W300 Yellow',
    manufacturer: 'Nikon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/coolpix-w300-yellow-1-s.jpg',
        image: '/img/products/photo/camera/coolpix-w300-yellow-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-w300-yellow-2-s.jpg',
        image: '/img/products/photo/camera/coolpix-w300-yellow-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-w300-yellow-3-s.jpg',
        image: '/img/products/photo/camera/coolpix-w300-yellow-3.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-w300-yellow-4-s.jpg',
        image: '/img/products/photo/camera/coolpix-w300-yellow-4.jpg',
      },
    ],
    quantity: 1200,
    type: 'camera',
    category: 'video-camera',
    price: 12399,
    salePrice: 14000,
    characteristics: [
      {
        title: 'display',
        value: '6,17 x 4,55',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'waterproof',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'optical',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '5x',
        isFilterable: true,
      },
    ],
  },
  {
    name: 'Coolpix P1000 Black',
    manufacturer: 'Nikon ',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        preview: '/img/products/photo/camera/coolpix-p1000-black-1-s.jpg',
        image: '/img/products/photo/camera/coolpix-p1000-black-1.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-p1000-black-2-s.jpg',
        image: '/img/products/photo/camera/coolpix-p1000-black-2.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-p1000-black-3-s.jpg',
        image: '/img/products/photo/camera/coolpix-p1000-black-3.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-p1000-black-4-s.jpg',
        image: '/img/products/photo/camera/coolpix-p1000-black-4.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-p1000-black-5-s.jpg',
        image: '/img/products/photo/camera/coolpix-p1000-black-5.jpg',
      },
      {
        preview: '/img/products/photo/camera/coolpix-p1000-black-6-s.jpg',
        image: '/img/products/photo/camera/coolpix-p1000-black-6.jpg',
      },
    ],
    quantity: 41,
    type: 'camera',
    category: 'photo',
    price: 27999,
    salePrice: 28500,
    characteristics: [
      {
        title: 'display',
        value: '6.2 х 4.6',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'super zoom',
        isFilterable: true,
      },
      {
        title: 'Matrix',
        value: 'CMOS',
        isFilterable: true,
      },
      {
        title: 'Stabilisation',
        value: 'optical',
        isFilterable: true,
      },
      {
        title: 'Optical zoom',
        value: '125х',
        isFilterable: true,
      },
    ],
  },
];

const lenses = [
  {
    name: 'EF 50mm f/1.4 USM',
    manufacturer: 'Nikon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/lens/ef-50mm-usm-1.jpg',
        preview: '/img/products/photo/lens/ef-50mm-usm-1_s.jpg',
      },
      {
        image: '/img/products/photo/lens/ef-50mm-usm-2.jpg',
        preview: '/img/products/photo/lens/ef-50mm-usm-2_s.jpg',
      },
      {
        image: '/img/products/photo/lens/ef-50mm-usm-3.jpg',
        preview: '/img/products/photo/lens/ef-50mm-usm-3_s.jpg',
      },
    ],
    quantity: 1,
    category: 'photo',
    type: 'lens',
    price: 10899,
    salePrice: null,
    characteristics: [
      {
        title: 'focus distance',
        value: '50',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'standard',
        isFilterable: true,
      },
      {
        title: 'Mass',
        value: '290',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: '70-300mm f/4.5-6.3 G ED VR AF-P DX',
    manufacturer: 'Nikon',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/lens/70-300-ed-vr-1.jpg',
        preview: '/img/products/photo/lens/70-300-ed-vr-1_s.jpg',
      },
      {
        image: '/img/products/photo/lens/70-300-ed-vr-2.jpg',
        preview: '/img/products/photo/lens/70-300-ed-vr-2_s.jpg',
      },
      {
        image: '/img/products/photo/lens/70-300-ed-vr-3.jpg',
        preview: '/img/products/photo/lens/70-300-ed-vr-3_s.jpg',
      },
    ],
    quantity: 12,
    category: 'photo',
    type: 'lens',
    price: 10799,
    salePrice: 8999,
    characteristics: [
      {
        title: 'focus distance',
        value: '70-300',
        isFilterable: true,
      },
      {
        title: 'type',
        value: 'tele',
        isFilterable: true,
      },
      {
        title: 'Mass',
        value: '415',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

const paper = [
  {
    name: 'PG-260',
    manufacturer: 'Colorway',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/paper/pg260-100-1.jpg',
        preview: '/img/products/photo/paper/pg260-100-1_s.jpg',
      },
      {
        image: '/img/products/photo/paper/pg260-100-1.jpg',
        preview: '/img/products/photo/paper/pg260-100-1_s.jpg',
      },
    ],
    quantity: 50,
    category: 'photo',
    type: 'paper',
    price: 546,
    salePrice: null,
    characteristics: [
      {
        title: 'Формат',
        value: 'А4',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'фотобумага',
        isFilterable: true,
      },
      {
        title: 'Количество листов в упаковке',
        value: '100',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'PSG-255',
    manufacturer: 'Colorway',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/paper/psg255-50-1.jpg',
        preview: '/img/products/photo/paper/psg255-50-1_s.jpg',
      },
      {
        image: '/img/products/photo/paper/psg255-50-2.jpg',
        preview: '/img/products/photo/paper/psg255-50-2_s.jpg',
      },
    ],
    quantity: 24,
    category: 'photo',
    type: 'paper',
    price: 499,
    salePrice: null,
    characteristics: [
      {
        title: 'Формат',
        value: 'А4',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'фотобумага',
        isFilterable: true,
      },
      {
        title: 'Количество листов в упаковке',
        value: '50',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

const memoryCards = [
  {
    name: 'UHS-I premium',
    manufacturer: 'Xoko',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/memory-card/UHS-I-premium-1.jpg',
        preview: '/img/products/memory-card/UHS-I-premium-1_s.jpg',
      },
      {
        image: '/img/products/memory-card/UHS-I-premium-2.jpg',
        preview: '/img/products/memory-card/UHS-I-premium-2_s.jpg',
      },
      {
        image: '/img/products/memory-card/UHS-I-premium-3.jpg',
        preview: '/img/products/memory-card/UHS-I-premium-3_s.jpg',
      },
    ],
    quantity: 50,
    category: 'memory cards',
    type: '-',
    price: 139,
    salePrice: null,
    characteristics: [
      {
        title: 'Объем памяти',
        value: '32GB',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'micro-SDНС',
        isFilterable: true,
      },
      {
        title: 'Cкорость чтения',
        value: '15 MB/s',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'Canvas Select+ A1',
    manufacturer: 'Kingston',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/memory-card/canvas-select-a1-1.jpg',
        preview: '/img/products/memory-card/canvas-select-a1-1_s.jpg',
      },
      {
        image: '/img/products/memory-card/canvas-select-a1-2.jpg',
        preview: '/img/products/memory-card/canvas-select-a1-2_s.jpg',
      },
    ],
    quantity: 50,
    category: 'memory cards',
    type: '-',
    price: 179,
    salePrice: null,
    characteristics: [
      {
        title: 'Объем памяти',
        value: '32GB',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'micro-SDНС',
        isFilterable: true,
      },
      {
        title: 'Cкорость чтения',
        value: '100 MB/s',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'EVO plus',
    manufacturer: 'Samsung',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/memory-card/evo-plus-1.jpg',
        preview: '/img/products/memory-card/evo-plus-1_s.jpg',
      },
      {
        image: '/img/products/memory-card/evo-plus-2.jpg',
        preview: '/img/products/memory-card/evo-plus-2_s.jpg',
      },
    ],
    quantity: 900,
    category: 'memory cards',
    type: '-',
    price: 299,
    salePrice: 249,
    characteristics: [
      {
        title: 'Объем памяти',
        value: '64GB',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'micro-SDXC',
        isFilterable: true,
      },
      {
        title: 'Cкорость чтения',
        value: '100 MB/s',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

const tripod = [
  {
    name: 'EX-630',
    manufacturer: 'VELBON',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/tripod/ex-630-1.jpg',
        preview: '/img/products/tripod/ex-630-1_s.jpg',
      },
      {
        image: '/img/products/tripod/ex-630-2.jpg',
        preview: '/img/products/tripod/ex-630-2_s.jpg',
      },
      {
        image: '/img/products/tripod/ex-630-3.jpg',
        preview: '/img/products/tripod/ex-630-3_s.jpg',
      },
      {
        image: '/img/products/tripod/ex-630-4.jpg',
        preview: '/img/products/tripod/ex-630-4_s.jpg',
      },
    ],
    quantity: 8,
    category: 'tripod',
    type: '-',
    price: 1899,
    salePrice: null,
    characteristics: [
      {
        title: 'Рабочая высота',
        value: '61.5 - 167.9 см',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'напольный',
        isFilterable: true,
      },
      {
        title: 'Вес',
        value: '1.7 кг',
        isFilterable: true,
      },
      {
        title: 'Нагрузка',
        value: '5 кг',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'Octopus SS-001 Black',
    manufacturer: 'XoKo',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/tripod/octopus-ss-001-black-1.jpg',
        preview: '/img/products/tripod/octopus-ss-001-black-1_s.jpg',
      },
      {
        image: '/img/products/tripod/octopus-ss-001-black-2.jpg',
        preview: '/img/products/tripod/octopus-ss-001-black-2_s.jpg',
      },
      {
        image: '/img/products/tripod/octopus-ss-001-black-3.jpg',
        preview: '/img/products/tripod/octopus-ss-001-black-3_s.jpg',
      },
      {
        image: '/img/products/tripod/octopus-ss-001-black-4.jpg',
        preview: '/img/products/tripod/octopus-ss-001-black-4_s.jpg',
      },
    ],
    quantity: 130,
    category: 'tripod',
    type: '-',
    price: 111,
    salePrice: null,
    characteristics: [
      {
        title: 'Рабочая высота',
        value: '2 - 24 см',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'настольный',
        isFilterable: true,
      },
      {
        title: 'Вес',
        value: '0.3 кг',
        isFilterable: true,
      },
      {
        title: 'Нагрузка',
        value: '0.6 кг',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'TT 150',
    manufacturer: 'BOSCH',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/tripod/tt-150-1.jpg',
        preview: '/img/products/tripod/tt-150-1_s.jpg',
      },
      {
        image: '/img/products/tripod/tt-150-2.jpg',
        preview: '/img/products/tripod/tt-150-2_s.jpg',
      },
      {
        image: '/img/products/tripod/tt-150-3.jpg',
        preview: '/img/products/tripod/tt-150-3_s.jpg',
      },
    ],
    quantity: 20,
    category: 'tripod',
    type: '-',
    price: 1700,
    salePrice: null,
    characteristics: [
      {
        title: 'Рабочая высота',
        value: '55 - 157 см',
        isFilterable: true,
      },
      {
        title: 'Тип',
        value: 'напольный',
        isFilterable: true,
      },
      {
        title: 'Вес',
        value: '1.3 кг',
        isFilterable: true,
      },
      {
        title: 'Нагрузка',
        value: '4 кг',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

const cases = [
  {
    name: 'LCS-EJC3',
    manufacturer: 'SONY',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/case/lcs-ejc3-1.jpg',
        preview: '/img/products/photo/case/lcs-ejc3-1_s.jpg',
      },
      {
        image: '/img/products/photo/case/lcs-ejc3-2.jpg',
        preview: '/img/products/photo/case/lcs-ejc3-2_s.jpg',
      },
      {
        image: '/img/products/photo/case/lcs-ejc3-3.jpg',
        preview: '/img/products/photo/case/lcs-ejc3-3_s.jpg',
      },
    ],
    quantity: 10,
    category: 'photo',
    type: 'case',
    price: 1000,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'кожа',
        isFilterable: true,
      },
      {
        title: 'Размер',
        value: '150 х 108 х 146',
        isFilterable: true,
      },
      {
        title: 'Предназначение',
        value: 'для зеркальных фотоаппаратов',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'SLRC201',
    manufacturer: 'CASE LOGIC',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/photo/case/slrc201-1.jpg',
        preview: '/img/products/photo/case/slrc201-1_s.jpg',
      },
      {
        image: '/img/products/photo/case/slrc201-2.jpg',
        preview: '/img/products/photo/case/slrc201-2_s.jpg',
      },
      {
        image: '/img/products/photo/case/slrc201-3.jpg',
        preview: '/img/products/photo/case/slrc201-3_s.jpg',
      },
      {
        image: '/img/products/photo/case/slrc201-4.jpg',
        preview: '/img/products/photo/case/slrc201-4_s.jpg',
      },
    ],
    quantity: 2,
    category: 'photo',
    type: 'case',
    price: 850,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'кожзаменитель',
        isFilterable: true,
      },
      {
        title: 'Размер',
        value: '209 х 108 х 146',
        isFilterable: true,
      },
      {
        title: 'Предназначение',
        value: 'для зеркальных фотоаппаратов',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

const externalHardDrive = [
  {
    name: 'Expansion Desktop 8TB USB 3.0',
    manufacturer: 'SEAGATE',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/externalHardDrive/expansion-desktop-1.jpg',
        preview: '/img/products/externalHardDrive/expansion-desktop-1_s.jpg',
      },
      {
        image: '/img/products/externalHardDrive/expansion-desktop-2.jpg',
        preview: '/img/products/externalHardDrive/expansion-desktop-2_s.jpg',
      },
    ],
    quantity: 10,
    category: 'externalHardDrive',
    type: '-',
    price: 5800,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'пластик',
        isFilterable: true,
      },
      {
        title: 'Вес',
        value: '950 г',
        isFilterable: true,
      },
      {
        title: 'Емкость',
        value: '8 Тб',
        isFilterable: true,
      },
      {
        title: 'Размер',
        value: '176 x 120.6 x 35.6 мм',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'SDSSDE61-500G-G25',
    manufacturer: 'SANDISK',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/externalHardDrive/sdssde61-500g-g25-1.jpg',
        preview: '/img/products/externalHardDrive/sdssde61-500g-g25-1_s.jpg',
      },
      {
        image: '/img/products/externalHardDrive/sdssde61-500g-g25-2.jpg',
        preview: '/img/products/externalHardDrive/sdssde61-500g-g25-2_s.jpg',
      },
      {
        image: '/img/products/externalHardDrive/sdssde61-500g-g25-3.jpg',
        preview: '/img/products/externalHardDrive/sdssde61-500g-g25-3_s.jpg',
      },
      {
        image: '/img/products/externalHardDrive/sdssde61-500g-g25-4.jpg',
        preview: '/img/products/externalHardDrive/sdssde61-500g-g25-4_s.jpg',
      },
    ],
    quantity: 10,
    category: 'externalHardDrive',
    type: '-',
    price: 5800,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'пластик',
        isFilterable: true,
      },
      {
        title: 'Вес',
        value: '600 г',
        isFilterable: true,
      },
      {
        title: 'Емкость',
        value: '500 гб',
        isFilterable: true,
      },
      {
        title: 'Размер',
        value: '100 x 90.6 x 35.6 мм',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'Backup Plus Ultra Touch 1TB USB-C',
    manufacturer: 'SEAGATE',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/externalHardDrive/backup-plus-ultra-touch-1.jpg',
        preview: '/img/products/externalHardDrive/backup-plus-ultra-touch-1_s.jpg',
      },
    ],
    quantity: 7,
    category: 'externalHardDrive',
    type: '-',
    price: 5800,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'пластик',
        isFilterable: true,
      },
      {
        title: 'Вес',
        value: '600 г',
        isFilterable: true,
      },
      {
        title: 'Емкость',
        value: '500 гб',
        isFilterable: true,
      },
      {
        title: 'Размер',
        value: '100 x 90.6 x 35.6 мм',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

const frame = [
  {
    name: 'DECO 13X18 PB42A-2 Venge',
    manufacturer: 'EVG',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/frame/deco-13x18-1.jpg',
        preview: '/img/products/frame/deco-13x18-1_s.jpg',
      },
    ],
    quantity: 200,
    category: 'photo',
    type: 'frame',
    price: 120,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'пластик',
        isFilterable: true,
      },
      {
        title: 'Установка',
        value: 'на ножке',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
  {
    name: 'DECO 15X20 PB04-1B Lilac',
    manufacturer: 'EVG',
    description: '<h1>Lorem ipsum</h1><p>Lorem ipsum dolor sit amet</p>',
    images: [
      {
        image: '/img/products/frame/deco-15x20-1.jpg',
        preview: '/img/products/frame/deco-15x20-1_s.jpg',
      },
    ],
    quantity: 140,
    category: 'photo',
    type: 'frame',
    price: 150,
    salePrice: null,
    characteristics: [
      {
        title: 'Материал',
        value: 'пластик',
        isFilterable: true,
      },
      {
        title: 'Установка',
        value: 'на ножке',
        isFilterable: true,
      },
    ],
    accessories: [],
  },
];

export const products = {
  cameras,
  lenses,
  paper,
  tripod,
  memoryCards,
  cases,
  externalHardDrive,
  frame,
};

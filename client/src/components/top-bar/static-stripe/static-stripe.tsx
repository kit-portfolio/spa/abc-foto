import React, {useState} from 'react';
import {data} from '../../../mock/top-bar/static-stripe.json';
import {ContentBox} from '../../../theme/common';
import {Section, Link} from './static-stripe.style';

/**
 * StaticStripe displays store static pages links.
 * @returns JSX.Element
 */
export default function StaticStripe(): JSX.Element {
  const [links] = useState(data);

  return (
    <Section>
      <ContentBox>
        {links.map((link) => <Link to={link.href} key={link.id}>{link.title}</Link>)}
      </ContentBox>
    </Section>
  );
}

import styled from 'styled-components';
import {PALETTE} from '../../../theme/theme';

export const Container = styled.div`
  background: ${PALETTE.WHITE};
  border: 3px solid ${PALETTE.DARK_RED};
  border-radius: 10px;
  width: 430px;
  margin: 0 auto;
  text-transform: uppercase;
`;

export const Header = styled.h2`
  font-weight: bold;
  font-size: 36px;
  background-color: ${PALETTE.DARK_RED};
  text-align: center;
  margin: 0;
  color: ${PALETTE.WHITE};
  padding-bottom: 5px;
`;

export const Message = styled.p`
  text-align: center;
  font-weight: bold;
  font-size: 22px;
  margin: 18px 28px;
`;

import React, {useState} from 'react';
import {useHistory, withRouter} from 'react-router';
import {WidgetLabel} from '../header.style';
import {CartWidgetWrapper, CartLogo, CartBadge} from './cart-widget.style';
import {URN} from '../../../../config/constants';

/**
 *
 * @remarks
 * @returns JSX.Element
 */
function CartWidget(): JSX.Element {
  const history = useHistory();
  const [count] = useState(5);

  /**
   *
   * @remarks
   * @returns void
   */
  function handleCartClick():void {
    history.push(URN.CART);
  }

  return <>
    <CartWidgetWrapper onClick={() => handleCartClick()}>
      <CartLogo/>
      <WidgetLabel>Корзина</WidgetLabel>
      {count && <CartBadge>{count}</CartBadge>}
    </CartWidgetWrapper>
  </>;
}

export default withRouter(CartWidget);

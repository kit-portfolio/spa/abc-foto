import * as Yup from 'yup';
import 'yup-phone';

export enum PLACEHOLDER {
  NAME = 'Example: John',
  SURNAME = 'Example: Doe',
  EMAIL = 'email@example.com',
  PHONE = 'Example: +38(050) 999 88 77',
  PASSWORD = '********',
  DD = 'DD',
  MM = 'MM',
  YYYY = 'YYYY',
  SEARCH = 'Например: Canon 400D',
}

/**
 * This function is intended to be used as a form submission stub
 * @param formData - values object from form controller
 * @param formReset - form reset function from form controller
 * @returns void
 */
export function submitStub(formData: any, formReset: any): void {
  console.log(formData);
  alert('STUB\nForm data is logged to console.');
  formReset();
}

export const nameValidationRules: Yup.BaseSchema = Yup.string()
    .matches(/^[A-Za-zА-Яа-яіІ]*$/, 'please enter valid name')
    .min(3, 'minimal length is 3 symbols')
    .max(40, 'maximum length is 40 symbols')
    .required('this field is required');

export const surnameValidationRules: Yup.BaseSchema = Yup.string()
    .matches(/^[A-Za-zА-Яа-яіІ]*$/, 'please enter valid name')
    .min(3, 'minimal length is 3 symbols')
    .max(40, 'maximum length is 40 symbols')
    .required('this field is required');

export const phoneValidationRules: Yup.BaseSchema = Yup.string()
    .phone('UA', true, 'enter valid phone number [UA]')
    .required('this field is required');

export const emailValidationRules: Yup.BaseSchema = Yup.string()
    .email('must be valid email')
    .required('this field is required');

/**
 * Helper function calculating maximum birthday date.
 * @returns Date string in 'YYYY-MM-DD' format.
 */
function getMaxBirthday() {
  const currentDate = new Date;
  return `${currentDate.getFullYear() - 18}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
}

export const birthdayValidationRules: Yup.BaseSchema = Yup.date()
    .min('1920-01-01', 'what are you? Vampire?')
    .max(getMaxBirthday(), 'you should be at least 18 yo.');

export const genderValidationRules: Yup.BaseSchema = Yup.string()
    .oneOf(['male', 'female'], 'there are only two variants');

export const passwordPolicy = /.*/;
export const passwordValidationRules: Yup.BaseSchema = Yup.string()
    .required('this field is required')
    .min(8, 'minimum length is 8 symbols')
    .matches(passwordPolicy, 'policy violation [policy shorthand]');

type passwordConfirmValidation = (passwordKey: string) => Yup.BaseSchema;
/**
 * Password confirmation validator
 * @param passwordKey - password input reference for password match check.
 * @returns Yup.BaseSchema
 */
export const passwordConfirmValidationRules: passwordConfirmValidation = function(passwordKey: string): Yup.BaseSchema {
  return Yup.string()
      .required('this field is required')
      .min(8, 'minimum length is 8 symbols')
      .oneOf([Yup.ref(passwordKey), null], 'wrong confirmation');
};

export const dayValidationRules: Yup.BaseSchema = Yup.number()
    .required('this field is required')
    .min(1, '1 is minimum')
    .max(31, '31 is maximum')
    .integer('sorry, no dots here');

export const monthValidationRules: Yup.BaseSchema = Yup.number()
    .required('this field is required')
    .min(1, '1 is minimum')
    .max(12, '12 is maximum')
    .integer('sorry, no dots here');

const currentYear = new Date().getFullYear();
export const yearValidationRules: Yup.BaseSchema = Yup.number()
    .required('this field is required')
    .min(1900, '1 is minimum')
    .max(currentYear, `${currentYear} is maximum`)
    .integer('sorry, no dots here');

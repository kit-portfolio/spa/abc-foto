import React from 'react';
import Header from './header/header';
import StaticStripe from './static-stripe/static-stripe';
import Breadcrumbs from './breadcrumbs/breadcrumbs';
import Categories from './categories/categories';

/**
 * Top bar container
 * @returns JSX.Element
 */
export default function TopBar(): JSX.Element {
  return (
    <section>
      <StaticStripe/>
      <Header/>
      <Categories/>
      <Breadcrumbs/>
    </section>
  );
};

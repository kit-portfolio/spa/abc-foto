import React from 'react';
import {useSelector} from 'react-redux';
import {TODO_ANY} from '../../../types/misc';
import {MODAL_ACTION} from '../../../store/modal-controller/modal-controller.actions';
import AuthModal from '../modals/auth-modal/auth-modal';
import InfoModal from '../modals/info-modal/info-modal';

interface DefaultRootState {
  modalController: TODO_ANY
}

/**
 * Component intended to control modals display and appearance
 * @returns JSX.Element
 */
export default function ModalController(): JSX.Element {
  const modalController = useSelector((state: DefaultRootState) => state.modalController);

  /**
   * Function defining modal to be rendered
   * @returns JSX.Element
   */
  function renderModal(): JSX.Element | null {
    switch (modalController.type) {
      case MODAL_ACTION.OPEN_AUTH_MODAL: return <AuthModal/>;
      case MODAL_ACTION.OPEN_INFO_MODAL: return <InfoModal/>;
      default: return null;
    }
  }

  return (
    <>
      {renderModal()}
    </>
  );
};

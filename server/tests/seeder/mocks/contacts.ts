export const contact = [
  {
    image: 'img/contacts/telephone.png',
    label: '0 (800) 555-212',
    href: 'tel:0800555212',
  },
  {
    image: 'img/contacts/lifecell.png',
    label: '(093) 555-13-13',
    href: 'tel:0935551313',
  },
  {
    image: 'img/contacts/kyivstar.png',
    label: '(050) 123-45-67',
    href: 'tel:0501234567',
  },
  {
    image: 'img/contacts/vodafone.png',
    label: '(099) 987-65-43',
    href: 'tel:0999876543',
    enabled: false,
  },
];

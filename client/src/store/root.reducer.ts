import {combineReducers} from 'redux';
import modalController from './modal-controller/modal-controller.reducer';
import auth from './auth/auth.reducer';
import user from './user/user.reducer';
import breadcrumbs from './breadcrumbs/breadcrumbs.reducer';

export const reducer = combineReducers({
  auth,
  user,
  breadcrumbs,
  modalController,
});

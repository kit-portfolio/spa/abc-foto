import React, {useState} from 'react';
import {useFormik} from 'formik';
import {
  LabelText,
  Label,
  TextInput,
  ValidationMessage,
  ActionButton,
  PasswordInput,
} from '../../../../../theme/form';
import {
  Form,
} from './auth-modal-form.style';
import * as Yup from 'yup';
import {
  PLACEHOLDER,
  nameValidationRules,
  phoneValidationRules,
  emailValidationRules,
  passwordValidationRules,
  passwordConfirmValidationRules,
} from '../../../../../config/form';
import {useDispatch} from 'react-redux';
import axios from 'axios';
import {logIn} from '../../../../../store/auth/auth.actions';
import {closeModal} from '../../../../../store/modal-controller/modal-controller.actions';
import {TODO_ANY} from '../../../../../types/misc';
import {toast} from 'material-react-toastify';
// @ts-ignore
import {ReactComponent as EyeIcon} from '../../../../../resources/eye.svg';
import {env} from '../../../../../config/environment';
import {ENDPOINT} from '../../../../../config/constants';


const SingUpValidationSchema = Yup.object().shape({
  email: emailValidationRules,
  password: passwordValidationRules,
  name: nameValidationRules,
  phoneNumber: phoneValidationRules,
  passwordConfirm: passwordConfirmValidationRules('password'),
});
/**
 * This component appears at authentication modal.
 * It's intended to collect data required for new user-widget registration.
 * @remarks Sends data to user-widget on submit. Change state auth-modal.
 * @returns JSX.Element
 */
export default function SignUp(): JSX.Element {
  const dispatch = useDispatch();
  const [isProtected, setIsProtected] = useState(true);
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      phoneNumber: '',
      password: '',
      passwordConfirm: '',
    },
    validationSchema: SingUpValidationSchema,
    onSubmit: (values, {setFieldError, resetForm}) => {
      switch (values.phoneNumber[0]) {
        case '3':
          values.phoneNumber = `+${values.phoneNumber}`;
          break;
        case '0':
          values.phoneNumber = `+38${values.phoneNumber}`;
          break;
      }
      axios.post(`${env.apiBaseUrl}${ENDPOINT.USERS}`,
          {
            firstName: values.name,
            email: values.email,
            phone: values.phoneNumber,
            password: values.password,
          })
          .then(() => {
            axios.post(`${env.apiBaseUrl}${ENDPOINT.LOGIN}`,
                {
                  email: values.email,
                  password: values.password,
                })
                .then((res) => {
                  logIn(dispatch, res.data.token);
                  resetForm();
                  dispatch(closeModal());
                })
                .catch(() => {
                  toast.error('Something went wrong on server');
                });
          },
          )
          .catch((e: TODO_ANY) => {
            if (!!e.response && e.response.data.field === 'Email') {
              setFieldError('email', e.response.data.message);
            } else {
              toast.error('Something went wrong on server');
            }
          });
    },
  });
  const error = formik.errors;
  const touched = formik.touched;
  return (
    <Form onSubmit={formik.handleSubmit}>
      <Label>
        <LabelText>
          Ваше имя
        </LabelText>
        {error.name && touched.name ?
          (<ValidationMessage>{error.name}</ValidationMessage>) :
          null}
        <TextInput
          name='name'
          type='text'
          value={formik.values.name}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.NAME}/>
      </Label>
      <Label>
        <LabelText>
          E-mail
        </LabelText>
        {error.email && touched.email ?
          (<ValidationMessage>{error.email}</ValidationMessage>) :
          null}
        <TextInput
          name='email'
          type='text'
          value={formik.values.email}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.EMAIL}/>
      </Label>
      <Label>
        <LabelText>
          Номер телефона
        </LabelText>
        {error.phoneNumber && touched.phoneNumber ?
          (<ValidationMessage>{error.phoneNumber}</ValidationMessage>) :
          null}
        <TextInput
          name='phoneNumber'
          type='text'
          value={formik.values.phoneNumber}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.PHONE}/>
      </Label>
      <Label>
        <LabelText>
          Пароль
        </LabelText>
        {error.password && touched.password ?
          (<ValidationMessage>{error.password}</ValidationMessage>) :
          null}
        <PasswordInput hidePassword={isProtected}>
          <input
            name='password'
            type={isProtected ? 'password' : 'text'}
            value={formik.values.password}
            onChange={formik.handleChange}
            placeholder={PLACEHOLDER.PASSWORD}/>
          <EyeIcon
            onMouseDown={() => setIsProtected(false)}
            onMouseUp={() => setIsProtected(true)}/>
        </PasswordInput>
      </Label>
      <Label>
        <LabelText>
          Подтверждение пароля
        </LabelText>
        {error.passwordConfirm && touched.passwordConfirm ?
          (<ValidationMessage>{error.passwordConfirm}</ValidationMessage>) :
          null}
        <PasswordInput hidePassword={isProtected}>
          <input
            name='passwordConfirm'
            type={isProtected ? 'password' : 'text'}
            value={formik.values.passwordConfirm}
            onChange={formik.handleChange}
            placeholder={PLACEHOLDER.PASSWORD}/>
          <EyeIcon
            onMouseDown={() => setIsProtected(false)}
            onMouseUp={() => setIsProtected(true)}/>
        </PasswordInput>
      </Label>
      <ActionButton onClick={() => formik.submitForm()}>
        Зарегистрироваться
      </ActionButton>
    </Form>
  );
}

export const orders = [
  {
    userId: '',
    comment: 'I have two number nine, number nine large, number six with extra dip,number seven, two number fourty-fifth, one with cheese and a large soda',
    client: {
      firstName: 'Валентина',
      lastName: 'Васильчук',
      phone: '0933369234',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Pickup',
      address: 'м. Івано-Франківськ, вул. Олександра Довженка 29',
      price: 30,
    },
    total: 11200,
    status: 'Pending',
  },
  {
    userId: '',
    comment: '-',
    client: {
      firstName: 'Алла',
      lastName: 'Таращук',
      phone: '0994447475',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id'],
    shipping: {
      type: 'Nova Poshta',
      address: 'м. Вінниця, вул. Юності 43А',
      price: 50,
    },
    total: 7000,
    status: 'Cancelled',

  },
  {
    userId: '',
    comment: 'as soon as possible',
    client: {
      firstName: 'Микола',
      lastName: 'Панасюк',
      phone: '+380678258612',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Pickup',
      address: 'м. Тернопіль, вул. Хрещатик 24Б',
      price: 65,
    },
    total: 40000,
    status: 'Completed',

  },
  {
    userId: '',
    comment: ' We\'re next month\'s garage sale fodder for sure.',
    client: {
      firstName: 'Олег',
      lastName: 'Середа',
      phone: '+380968261689',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Nova Poshta',
      address: 'м. Черкасси, вул. Пастерівська 31',
      price: 50,
    },
    total: 14250,
    status: 'Pending',

  },
  {
    userId: '',
    comment: 'To infinity and beyond!',
    client: {
      firstName: 'Милана',
      lastName: 'Зайцева',
      phone: '+7 (958) 298-94-10',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Courier',
      address: 'м.Харків, вул. Сім\'ї Сосніних 124А',
      price: 50,
    },
    total: 4580,
    status: 'Pending',

  },
  {
    userId: '',
    comment: '-',
    client: {
      firstName: 'Віталій',
      lastName: 'Лисенко',
      phone: '+380507396444',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Nova Poshta',
      address: 'м.Харків, вул. Гоголя 124А',
      price: 100,
    },
    total: 18500,
    status: 'Completed',

  },
  {
    userId: '',
    comment: 'Мальчик, водочки нам принеси. Мы домой летим',
    client: {
      firstName: 'Наташа',
      lastName: 'Захарчук',
      phone: '+380934856085',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Courier',
      address: 'м.Львів, вул. Шевченка 3А',
      price: 75,
    },
    total: 78450,
    status: 'Pending',

  },
  {
    userId: '',
    comment: '-',
    client: {
      firstName: 'Данило',
      lastName: 'Петренко',
      phone: '0672843111',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id'],
    shipping: {
      type: 'Nova Poshta',
      address: 'м.Чернівці, вул. Янгеля 34А',
      price: 25,
    },
    total: 340,
    status: 'Completed',

  },
  {
    userId: '',
    comment: 'Да, человек смертен, но это было бы еще полбеды. Плохо то, что он иногда внезапно смертен, вот в чем фокус',
    client: {
      firstName: 'Інна',
      lastName: 'Шинкаренко',
      phone: '0676231880',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id'],
    shipping: {
      type: 'Courier',
      address: 'м.Київ, вул.Байди Вишневецького 34В',
      price: 100,
    },
    total: 3748,
    status: 'Cancelled',

  },
  {
    userId: '',
    comment: 'дура с сердцем и без ума такая же несчастная дура, как и дура с умом без сердца',
    client: {
      firstName: 'Світлана',
      lastName: 'Павлюк',
      phone: '0505030281',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'PICKUP',
      address: 'м. Житомир, вул. Київська 93',
      price: 40,
    },
    total: 100000,
    status: 'Pending',

  },
  {
    userId: '',
    comment: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
    client: {
      firstName: 'Анастасія',
      lastName: 'Іванченко',
      phone: '+380963431658',
      email: 'mock@example.org',
    },
    products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
    shipping: {
      type: 'Nova Poshta',
      address: 'м. Біла Церква, вул. Северина Наливайка 20А',
      price: 50,
    },
    total: 180000,
    status: 'Pending',
  },
];

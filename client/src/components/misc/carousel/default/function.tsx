import {Dots} from './default-carousel.style';
import React, {Dispatch, SetStateAction} from 'react';
import ICover from '../../../../types/cover';

export interface ICONFIGURATION{
  STRIDE_LENGTH: number,
  STEP: number,
  SHIFT: string,
  CONTENT_WIDTH: string,
  ANIMATION_DURATION: string,
  MARGIN?: string,
  PADDING?: string,
  PADDING_FOR_SHADOW?: string,
}

export enum BUTTON_TYPE {
  NEXT = 'Next',
  PREV = 'Prev'
}

interface IHandleEventDirectionController {
  position: number,
  type: string,
  maxDisplacement: number,
  setPosition: Dispatch<SetStateAction<number>>,
  step: number,
  strideLength: number,
}

/**
 * This function handles click on SwipeButton and changes state depending on the type of the pressed button
 * @returns void
 */
export function handleEventDirectionController(props: IHandleEventDirectionController): void {
  const {position, maxDisplacement, setPosition, type, step, strideLength} = props;
  if ((type === BUTTON_TYPE.NEXT)) {
    if (position < maxDisplacement) {
      setPosition(position + (step * strideLength));
    } else {
      setPosition(0);
    }
  } else if (type === BUTTON_TYPE.PREV) {
    if (position > 0) {
      setPosition(position - (step * strideLength));
    } else {
      setPosition(maxDisplacement);
    }
  } else return;
}


interface IDotsGeneratorProps {
  items: ICover[],
  position: number,
  setPosition: Dispatch<SetStateAction<number>>,
}

/**
 * This function generates clickable dots inside the slider.
 * @returns JSX.Element[]
 */
export function dotsGenerator(props: IDotsGeneratorProps) : JSX.Element[] {
  const {items, position, setPosition} = props;
  return items.map((item, i): JSX.Element => {
    return (
      <Dots key={item._id}
        isActive={i * 1200 === position}
        onClick={() => setPosition(i * 1200)}/>
    );
  });
}



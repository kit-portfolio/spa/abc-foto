/**
 * Helper function verifying if string provided is JSON.
 * @param str - string to be verified.
 * @returns boolean
 */
export function isJSON(str): boolean {
  try {
    const obj = JSON.parse(str);
    if (obj && typeof obj === 'object') {
      return true;
    }
  } catch (err) {}
  return false;
}

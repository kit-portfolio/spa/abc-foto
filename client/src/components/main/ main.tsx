import React from 'react';
import Routes from '../../routes/routes';
import {Section} from './ main.style';

/**
 * Main is the component intended to display store pages
 * @returns JSX.Element
 */
export default function Main(): JSX.Element {
  return (
    <Section>
      <Routes/>
    </Section>
  );
}

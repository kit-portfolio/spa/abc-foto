import _ from 'lodash';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {Request, Response} from 'express';
import keys from '../config/keys';
import {queryCreator} from '../commonHelpers/queryCreator';
import message, {ENTITY, OPERATION} from '../config/messages';
import {validateRegistrationForm} from '../validation/validationHelper';
import User, {IUser} from '../models/User.model';

enum FIELD{
  PASSWORD = 'Password',
  EMAIL = 'Email'
}

interface IUserRequest extends Request {
  user: IUser
}

/**
 * Create new user
 * @route   POST /user
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function createUser(rq: Request, rs: Response): void {
  const initialQuery = _.cloneDeep(rq.body);
  const {errors, isValid} = validateRegistrationForm(rq.body);

  if (!isValid) {
    rs.status(400).json(errors);
    return;
  }

  User.findOne({email: rq.body.email})
      .then((user) => {
        if (user) {
          if (user.email === rq.body.email) {
            return rs
                .status(400)
                .json({message: message.errors.fieldSpecific.emailIsTaken, field: FIELD.EMAIL});
          }
        }
        const newUser = new User(queryCreator(initialQuery));
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) {
              rs
                  .status(400)
                  .json({message: message.errors.generic.serverError});

              return;
            }

            newUser.password = hash;
            newUser
                .save()
                .then((user) => rs.status(201).json(user))
                .catch((e) =>
                  rs.status(400).json({
                    message: message.entityManipulation.error(ENTITY.USER, OPERATION.CREATE),
                    error: e,
                  }),
                );
          });
        });
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Login user
 * @route   POST /user/login
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 * @remarks Response contains JWT Token
 */
export async function loginUser(rq: Request, rs: Response): Promise<void> {
  const {errors, isValid} = validateRegistrationForm(rq.body);
  if (!isValid) {
    rs.status(400).json(errors);
    return;
  }

  const email = rq.body.email;
  const password = rq.body.password;

  User.findOne({email: email})
      .then((user) => {
        if (!user) {
          return rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.USER),
            field: FIELD.EMAIL,
          });
        }

        // Check Password
        bcrypt.compare(password, user.password).then((isMatch) => {
          if (isMatch) {
            // User Matched
            const payload = {
              id: user.id,
              isAdmin: user.isAdmin,
            }; // Create JWT Payload

            // Sign Token
            jwt.sign(
                payload,
                keys.secretOrKey,
                {expiresIn: 36000},
                (err, token) => {
                  rs.json({
                    success: true,
                    token: 'Bearer ' + token,
                  });
                },
            );
          } else {
            rs.status(401).json({message: message.errors.fieldSpecific.wrongPass, field: FIELD.PASSWORD});
          }
        });
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Return current user
 * @route   GET /
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getUser(rq: IUserRequest, rs: Response): void {
  rs.json(rq.user);
}

/**
 * Edit current user
 * @route   PUT /user
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function editUserInfo(rq: IUserRequest, rs: Response): void {
  const initialQuery = _.cloneDeep(rq.body);
  const {errors, isValid} = validateRegistrationForm(rq.body);
  let brake: boolean = false;
  if (!isValid) {
    rs.status(400).json(errors);
    return;
  }
  User.findById(rq.user.id)
      .then(async (currentUser) => {
        if (currentUser && currentUser.email !== rq.body.email) {
          await User.findOne({email: rq.body.email})
              .then((user) => {
                if (user) {
                  brake = true;
                  return rs.status(400).json({
                    message: message.errors.fieldSpecific.emailIsTaken,
                    field: FIELD.EMAIL,
                  });
                }
              })
              .catch(() => {
                return rs.status(404).json({
                  message: message.errors.entitySpecific.notFound(ENTITY.USER),
                });
              });
        }
        const updatedUser = queryCreator(initialQuery);
        if (brake) {
          return;
        }
        User.findOneAndUpdate(
            {_id: rq.user.id},
            {$set: updatedUser},
            {new: true},
        )
            .then((user) => rs.json(user))
            .catch((e) =>
              rs.status(400).json({
                message: message.entityManipulation.error(ENTITY.USER, OPERATION.UPDATE),
                error: e,
              }),
            );
      });
}

/**
 * Change current user password
 * @route   POST /user/profile/update-password
 * @remarks response contains current user and password change status message
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function updatePassword(rq: IUserRequest, rs: Response): void {
  const {errors, isValid} = validateRegistrationForm(rq.body);
  if (!isValid) {
    rs.status(400).json(errors);
    return;
  }

  User.findOne({_id: rq.user.id}, (err, user) => {
    bcrypt.compare(rq.body.oldPassword, user.password)
        .then((isMatch) => {
          if (!isMatch) {
            rs.status(401)
                .json({message: message.errors.fieldSpecific.passwordNoMatch, field: FIELD.PASSWORD});
          } else {
            let newPassword = rq.body.newPassword;

            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(newPassword, salt, (err, hash) => {
                if (err) throw err;
                newPassword = hash;
                User.findOneAndUpdate(
                    {_id: rq.user.id},
                    {
                      $set: {
                        password: newPassword,
                      },
                    },
                    {new: true},
                )
                    .then((customer) => {
                      rs.json({
                        message: message.status.passwordUpdated,
                        customer: customer,
                      });
                    })
                    .catch((e) =>
                      rs.status(400).json({
                        message: message.errors.generic.serverError,
                        error: e,
                      }),
                    );
              });
            });
          }
        });
  });
}

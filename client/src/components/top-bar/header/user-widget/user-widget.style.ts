import styled from 'styled-components';
import {widgetPictogramDimensions, WidgetWrapper} from '../header.style';
// @ts-ignore
import {ReactComponent as UserIcon} from '../../../../resources/user.svg';
// @ts-ignore
import {PALETTE} from '../../../../theme/theme';

export const UserWidgetWrapper = styled(WidgetWrapper)`
  margin-right: 20px;
`;

interface IUserLogo {
  isLoggedIn: boolean,
}

export const UserLogo = styled(UserIcon)<IUserLogo>`
  ${widgetPictogramDimensions};
  fill: ${(props) => props.isLoggedIn ? PALETTE.APPLE : PALETTE.NOBEL};
  margin-right: 10px;
`;

export const UserAvatar = styled.img`
  ${widgetPictogramDimensions};
  border-radius: 50%;
`;

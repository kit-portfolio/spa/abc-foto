export const covers = [
  {
    href: '/img/cover/ambush.png',
    link: '/first-carousel',
    alt: 'pasta',
  },
  {
    href: '/img/cover/big-bro.png',
    link: '/second-carousel',
    alt: 'pasta',
  },
  {
    href: '/img/cover/canon-bag-offer.png',
    link: '/third-carousel',
    alt: 'pasta',
  },
];

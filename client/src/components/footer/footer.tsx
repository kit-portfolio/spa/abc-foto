// Modules
import React from 'react';
import {Link} from 'react-router-dom';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import {toast} from 'material-react-toastify';
// Theme styles
import {ContentBox, simpleForm as Form} from '../../theme/common';
// Component resources
import {data as footerData} from '../../mock/footer.json';
import Logo from '../misc/logo/logo';
import {
  Divider,
  Grid,
  FooterLink,
  Section,
  SocialMediaBox,
  ValidationMessage,
  Text,
  SmallText,
  footerLinksStyle,
} from './footer.style';
import FooterStaticLinks from './footer-static/footer-static';
// @ts-ignore
import {ReactComponent as SubscribeButton} from '../../resources/chevron.svg';
// @ts-ignore
import {ReactComponent as FacebookButton} from '../../resources/facebook.svg';
// @ts-ignore
import {ReactComponent as InstagramButton} from '../../resources/instagram.svg';
import {PLACEHOLDER, emailValidationRules} from '../../config/form';
import {TODO_ANY} from '../../types/misc';
import {env} from '../../config/environment';
import {ENDPOINT} from '../../config/constants';

const axios = require('axios');
const SubscriptionSchema = Yup.object().shape({
  email: emailValidationRules,
});

/**
 * Footer - functional area located at the bottom of the App
 * @returns JSX.Element
 */
export default function Footer(): JSX.Element {
  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: SubscriptionSchema,
    onSubmit: (values, {resetForm, setFieldError}) => {
      axios.post(`${env.apiBaseUrl}${ENDPOINT.SUBSCRIPTION}`, {
        email: values.email,
      })
          .then(() => {
            resetForm();
            toast.success('Subscription successful');
          })
          .catch((e: TODO_ANY) => {
            setFieldError('email', e.response.data.message);
          });
    },
  });

  return (
    <Section>
      <ContentBox>
        <Grid>
          <div>
            <Logo/>
            <SmallText style={footerLinksStyle()}>Интернет магазин</SmallText>
            <FooterLink to="tel:0800212150">0 (800) 21-21-50</FooterLink>
            <FooterLink to="mailto:info@abcphoto.com.ua">info@abcphoto.com.ua</FooterLink>
          </div>
          {footerData.map((item) => <FooterStaticLinks content={item} key={item.id}/>)}
          <div>
            <Text style={footerLinksStyle(true)}>Подписаться на рассылку</Text>
            <Form onSubmit={formik.handleSubmit}>
              <input
                name='email'
                type='text'
                value={formik.values.email}
                onChange={formik.handleChange}
                placeholder={PLACEHOLDER.EMAIL}/>
              <SubscribeButton onClick={formik.submitForm}/>
            </Form>
            {formik.errors.email && formik.touched.email ?
              (<ValidationMessage>{formik.errors.email}</ValidationMessage>) :
              (<ValidationMessage style={{opacity: 0}}/>)}
            <SocialMediaBox>
              Мы в социальных сетях
              <Link to='#'><FacebookButton/></Link>
              <Link to='#'><InstagramButton/></Link>
            </SocialMediaBox>
          </div>
        </Grid>
        <Divider/>
        <SmallText style={footerLinksStyle(true)}>
          © abcphoto 2020 - Все права защищены.
        </SmallText>
      </ContentBox>
    </Section>
  );
}

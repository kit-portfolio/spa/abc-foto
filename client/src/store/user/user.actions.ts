import {TODO_ANY} from '../../types/misc';
import {useDispatch} from 'react-redux';
import {IUser} from '../../types/user';
import {toast} from 'material-react-toastify';
import {env} from '../../config/environment';
import {ENDPOINT} from '../../config/constants';

const axios = require('axios');

export enum USER_ACTION {
  FETCH = 'FETCH USER',
  WRITE = 'WRITE USER',
  FORGET = 'FORGET USER'
}

export type UserStore = IUser | {};

export interface IUserAction {
  type: USER_ACTION,
  payload: UserStore,
}

/**
 * Helper function intended to handle user fetching process.
 * @returns void
 */
export function useFetchUser(token: string): void {
  const dispatch = useDispatch();

  axios.get(`${env.apiBaseUrl}${ENDPOINT.USERS}`, {
    headers: {'Authorization': token},
  })
      .then((res: TODO_ANY) => dispatch(createWriteUserAction(res.data)))
      .catch((e: TODO_ANY) => {
        console.log(e);
        toast.error(`Can't get information about user`);
      });
}


/**
 * Helper function intended to create "USER_ACTION.WRITE" object of valid shape.
 * @returns IUserAction object
 */
export function createWriteUserAction(payload: IUser): IUserAction {
  return {
    type: USER_ACTION.WRITE,
    payload: {
      firstName: payload.firstName,
      lastName: payload.lastName,
      email: payload.email,
      phone: payload.phone,
      gender: payload.gender,
      avatarUrl: payload.avatarUrl,
      birthDate: (payload.birthDate) ? new Date(payload.birthDate).toISOString().split('T')[0] : null,
    },
  };
}

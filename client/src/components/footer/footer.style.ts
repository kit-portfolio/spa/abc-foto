import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {PALETTE, FONT_SIZE} from '../../theme/theme';
import {CSSProperties} from 'react';

export const Section = styled.section`
  color: ${PALETTE.WHITE};
  background-color: ${PALETTE.BLACK};
  padding-top: 75px;
`;

export const Grid = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1.5fr;
  grid-template-rows: 1fr;
  gap: 55px;
  margin-bottom: 60px;
`;

export const FooterLink = styled(Link)`
  color: white;
  text-decoration: none;
  text-align: left;
  display: block;
  font-size: 12px;
  padding: 10px 0;
  transition: 0.3s;

  &:hover {
    color: #006600;
    transition: 0.3s;
  }
`;

export const SocialMediaBox = styled.div`
  margin-top: 8px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export const ValidationMessage = styled.p`
  ${FONT_SIZE.SMALL_TEXT};
  color: ${PALETTE.ALIZARIN_CRIMSON};
  height: 12px;
  display: block;
`;

export const Divider = styled.hr`
  height: 1px;
  border: none;
  background-color: ${PALETTE.PALE_SKY};
`;

export const Text = styled.p`
  ${FONT_SIZE.TEXT};
  font-weight: bold;
  color: ${PALETTE.WHITE};
`;

export const SmallText = styled.p`
  ${FONT_SIZE.SMALL_TEXT};
  color: ${PALETTE.WHITE};
`;

/**
 * Helper function intended to define a  custom style for footer links.
 * @returns generic footer link style object.
 */
export function footerLinksStyle(center: boolean = false):CSSProperties {
  return {
    margin: '15px 0 20px',
    textAlign: center ? 'center' : 'initial',
  };
}

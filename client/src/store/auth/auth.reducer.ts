import {TODO_ANY} from '../../types/misc';
import {AUTH_ACTION} from './auth.actions';

const loggedOut = {isLoggedIn: false};

const initialState = loggedOut;

const reducer = (state = initialState, action: TODO_ANY) => {
  switch (action.type) {
    case AUTH_ACTION.LOG_IN:
      return action.payload;
    case AUTH_ACTION.LOG_OUT:
      return loggedOut;
    default:
      return state;
  }
};

export default reducer;

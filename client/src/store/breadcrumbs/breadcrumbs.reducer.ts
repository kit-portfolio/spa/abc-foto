import {TODO_ANY} from '../../types/misc';
import {BREADCRUMBS_ACTION, BreadcrumbsStore} from './breadcrumbs.actions';

const initialState: BreadcrumbsStore = [];

const reducer = (state = initialState, action: TODO_ANY): BreadcrumbsStore => {
  switch (action.type) {
    case BREADCRUMBS_ACTION.CART:
      return [{
        label: 'Моя корзина',
      }];
    case BREADCRUMBS_ACTION.CHECKOUT:
      return [{
        label: 'Оформление заказа',
      }];
    case BREADCRUMBS_ACTION.PROFILE:
      return [{
        label: 'Мой профиль',
      }];
    case BREADCRUMBS_ACTION.CLEAR:
      return [];
    default:
      return state;
  }
};

export default reducer;
